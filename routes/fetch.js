let express = require('express');
let router = express.Router();
let request = require('request');
let cheerio = require('cheerio');
var fetch_tshirt = require('../fetch_req/fetch_tshirt.js');

// route for the fetching data from http client Flipkart
router.get('/flipkart/mobile', function (req, res, next) {

    url = 'https://www.flipkart.com/mobiles/samsung~brand/pr?count=40&p%5B%5D=sort%3Drecency_desc&sid=tyy%2F4io&wid=1.productCard.PMU_V2';

    request(url, function (error, response, html) {
        if (!error) {
            let $ = cheerio.load(html);

            let json = [];
            $('._3O0U0u').filter(function () {
                let data = $(this);

                json.push({
                    "Model": data.children().find('._3wU53n').text(),
                    'Rating': data.children().find('.hGSR34').text(),
                    'Price': data.children().find('._1vC4OE').text(),
                    'Details': data.children().find('.vFw0gD').text()

                });
            })
            res.json(json)
        }
    })
});

// route for the fetching data from http client Snapdeal
router.get('/snapdeal/tshirt', function (req, res, next) {

    fetch_tshirt('https://www.snapdeal.com/products/mens-tshirts-polos').then((data) => {
        res.json(data)
    })
});

// route for the fetching data from http client Flipkart

router.get('/flipkart/mobile/full', function (req, res, next) {

    url = "https://www.flipkart.com/mobiles/mi~brand/pr?sid=tyy,4io&otracker=nmenu_sub_Electronics_0_Mi"; //for MI's mobile
    // url = 'https://www.flipkart.com/mobiles/samsung~brand/pr?count=40&p%5B%5D=sort%3Drecency_desc&sid=tyy%2F4io&wid=1.productCard.PMU_V2';        //For Samsung brand
    // url='https://www.flipkart.com/mobiles/pr?sid=tyy%2F4io&p%5B%5D=facets.brand%255B%255D%3DApple&p%5B%5D=facets.availability%255B%255D%3DExclude%2BOut%2Bof%2BStock&otracker=clp_metro_expandable_1_apple-categ-94e9d_iPhone_apple-products-store_90ff40fd-a46b-4a40-9440-fbe783136afb_DesktopSite_wp1&fm=neo/merchandising&iid=M_6732c239-11d4-4327-bcc1-62a155b2565d_1.90ff40fd-a46b-4a40-9440-fbe783136afb_DesktopSite&ppt=CLP&ppn=CLP:mobile-phones-store';     //for Apple brand

    fetchMobile(url);

    async function fetchMobile(url) {

        request(url, function (error, response, html) {
            if (!error) {
                let $ = cheerio.load(html);
                let product = [];

                let total_data = $('._3O0U0u').length
                $('._3O0U0u').each(function () {
                    let data = $(this);
                    fetchDetails("https://www.flipkart.com" + data.children().find('._31qSD5').attr("href"), function (details_data) {
                        product.push({
                            "Title": data.children().find('._3wU53n').text(),
                            'Rating': data.children().find('.hGSR34').text(),
                            'Price': data.children().find('._1vC4OE').text(),
                            'Details': details_data,
                        });
                        total_data--;
                        if (!total_data) {
                            res.json(product);
                        }
                    })
                });
            }
        });
    }
    async function fetchDetails(item, callback) {

        request(item, function (error, response, html) {
            if (!error) {
                let $ = cheerio.load(html);

                let product_details = [];

                $('._2Cl4hZ').filter(function () {
                    let data = $(this);
                    product_details.push({
                        "Model": data.children().find('._35KyD6').text(),
                        "Discount": data.children().find('._2XDAh9 span').text(),
                        "bank offer": data.children().find('._2YJn2R + span').text(),
                        "color": data.children().find('.CunQIh').text(),
                        "Highlights": data.children().find('._2-riNZ').text(),
                        "Seller": data.children().find('#sellerName').children("span").children("span").text(),
                        "Replacement policies": data.children().find('._20PGcF').text(),
                        "In the box": data.children().find('._3YhLQA').first().text(),
                        "Warranty": data.children().find('._3YhLQA').last().text(),

                    });
                });
                callback(product_details);
            }
        });
    }
});
module.exports = router;