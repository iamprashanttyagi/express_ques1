var express = require('express');
var router = express.Router();
var md5 = require('md5');
let users = require('../authenticate/auth_user_schema.js');
let token_data = require('../authenticate/auth_token_schema.js');
var auth_token = require('../token_verify/token_verify.js');
var valid_data = require('../validate/user_address_data_validate.js');
let address_data = require('../authenticate/auth_address_schema.js');
let model = require('../authenticate/auth_model.js');
var jwt = require('jsonwebtoken');
var Secret = 'xyz';
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
router.post('/register', function (req, res, next) {
    var username = req.body.username;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var email = req.body.email;
    var pass = req.body.pass;
    var conpass = req.body.conpass;

    var new_users = new users({
        'username': username,
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'pass': md5(pass)
    });


    if (pass === conpass) {
        new_users.save(function (err) {
            if (err) {
                res.status(400).json({
                    err: err.message
                })
            } else {
                console.log('users Saved');
                res.json({
                    message: 'Users data is saved in the database'
                });
            }
        });
    } else {

        console.log('Password are not same...');
        res.send('Password are not same...');
    }
});



passport.use(new LocalStrategy(
    function (username, password, done) {
        users.getUserByUsername(username, function (err, user) {
            if (err) {
                console.log(err);
                throw err;
            }

            if (!user) {
                console.log("username not available")
                return done(null, false, {
                    message: 'Unknown User'
                });
            }
            users.comparePassword(password, user.pass, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    const payload = {
                        admin: user._id
                    };
                    var token = jwt.sign(payload, Secret, {
                        expiresIn: 86400
                    });
                    var token_detail = ({
                        success: true,
                        message: 'Login Successfully done..... Enjoy your token!',
                        token: token
                    });
                    console.log(token_detail)
                    return done(null, user)

                } else {
                    return done(null, false, {
                        message: 'Invalid password'
                    });
                }
            });
        });
    }));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    users.getUserById(id, function (err, user) {
        done(err, user);
    });
});

// Route for the login....
router.post('/login', passport.authenticate('local', {
    failureRedirect: 'unsuccess',
    failureFlash: true
}), function (req, res, next) {
    res.redirect('success');
});

// route to get user data

router.get('/get', auth_token, function (req, res) {
    if (req.param('id') != '' && req.param('id') != null) {
        users.find({
            _id: req.param('id')
        }).select('username firstname lastname email').
        populate('addresses', 'address city state pincode phone').exec(function (err, result) {
            if (err) {
                res.json({
                    err: err.message
                });
                return;
            };
            res.json(result);
        })
    } else {
        res.json({
            success: false,
            message: "Enter parameter which is id."
        })
    }

});

// Route for delete using PUT type

router.put('/delete', auth_token, function (req, res) {

    users.deleteOne({
        _id: req.user
    }, function (err) {
        if (err) return handleError(err, req, res);
        res.json({
            success: true,
            message: "User Deleted whose id was : " + req.user,
        })
    });
});

// route for the getting data in pages

router.get('/list', function (req, res) {
    if (req.param('page') != '' && req.param('page') != null) {
        var pageOptions = {
            page: req.param('page') || 0,
            limit: 10
        }

        users.find()
            .skip(pageOptions.page * pageOptions.limit)
            .limit(pageOptions.limit)
            .exec(function (err, result) {
                if (err) {
                    res.json(err);
                    return;
                };
                res.json(result);
            })
    } else {
        res.json({
            success: false,
            message: "Enter parameter which is page no."
        })
    }
});

//Route for the adding address for the user::
router.post('/address', auth_token, function (req, res, next) {
    var user_id = req.user;
    var valid_user_address_data = valid_data(req.body) // here req.user have the value of user id which is coming after the verifing the token
    if (valid_user_address_data instanceof Error) {
        return res.json({
            status: false,
            message: valid_user_address_data.message
        })
    } else {
        var new_address = new address_data({
            'user_id': user_id,
            'address': valid_user_address_data.address,
            'city': valid_user_address_data.city,
            'state': valid_user_address_data.state,
            'pincode': valid_user_address_data.pincode,
            'phone': valid_user_address_data.phone
        });
        new_address.addressOf = user_id;
        new_address.save(function (err) {
            if (err) {
                res.status(400).json({
                    err: err.message
                })
            } else {
                users.findOneAndUpdate({
                    _id: user_id
                }, {
                    $push: {
                        addresses: new_address
                    }
                }, function (err, update_query_res) {
                    if (err) return res.send(err);
                    else {
                        res.json({
                            status: true,
                            message: 'Users address has been saved in the database'
                        });
                    }
                });
            }
        });
    }
});

// Login sucessfull or unsucessfull

router.get('/success', function (req, res) {
    res.json({
        status: true,
        message: "You are successfully login check token on console"
    })
});

router.get('/unsuccess', function (req, res) {
    res.json({
        status: false,
        message: "unable to login, try again... check username or password"
    })
});


// get the users if user is authenticated with passport

router.get('/getuser', checkAuthentication, function (req, res) {
    users.find().select('username firstname lastname email').exec(function (err, result) {
        if (err) {
            res.json(err);
            return;
        };
        res.json(result);
    })
});

function checkAuthentication(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.json({
            status: false,
            message: "Not authenticated !...... plz login"
        })
    }
}

module.exports = router;