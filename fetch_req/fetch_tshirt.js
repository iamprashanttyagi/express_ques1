let request = require('request');
let cheerio = require('cheerio');

const fetch_tshirt = (url) => {
    return new Promise((resolve, reject) => {
        request(url, function (error, response, html) {
            if (!error) {
                let $ = cheerio.load(html);

                let json = [];
                $('.product-title').filter(function () {
                    let data = $(this);

                    json.push({
                        "Title": data.text(),
                        "Price before discount": data.parent().children().find('.product-desc-price').text(),
                        "price after discount": data.parent().children().find('.product-price').text(),
                        "Discount": data.parent().children().find('.product-discount span').text(),
                    });
                })
                resolve(json);
            }
        })
    })
};

module.exports = fetch_tshirt;