var jwt = require('jsonwebtoken');
var Secret = 'xyz';

function verifyToken(req, res, next) {
    var token = req.headers.token || req.query.token || req.headers['x-access-token'];

    if (!token)
        return res.send({
            auth: false,
            message: 'No token provided.'
        });

    jwt.verify(token, Secret, function (err, decoded) {
        if (err)
            return res.send({
                auth: false,
                message: 'Failed to authenticate token.'
            });

        req.user = decoded.admin;
        next();
    });
}
module.exports = verifyToken;