var mongoose = require('mongoose');
var conn = mongoose.createConnection('mongodb://127.0.0.1/user_details_db');

var token_schema = mongoose.Schema({
    user_id_at_tokenData: {
        type: String,
        required: true
    },
    access_token: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        required: true
    },
    expiry_date: {
        type: Date,
        required: true
    },

}, {

    strict: false,
    collection: 'token_data'
});


module.exports = conn.model("token_data", token_schema)