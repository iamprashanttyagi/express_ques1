var mongoose = require('mongoose');
var conn = mongoose.createConnection('mongodb://127.0.0.1/user_details_db');

var address_schema = mongoose.Schema({
    user_id: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    pincode: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    addressOf: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    }

}, {

    strict: false,
    collection: 'address_data'
});


module.exports = conn.model("address_data", address_schema)