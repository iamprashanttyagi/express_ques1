	var mongoose = require('mongoose');
	var conn = mongoose.createConnection('mongodb://127.0.0.1/user_details_db');
	var uniqueValidator = require('mongoose-unique-validator');
	var md5 = require('md5');

	var users_schema = mongoose.Schema({
		firstname: {
			type: String,
			required: true
		},
		lastname: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true,
			unique: [true, "email must be unique"]
		},

		username: {
			type: String,
			lowercase: true,
			required: true,
			unique: [true, "email must be unique"],
			trim: true,
			validate: {
				validator: function (v) {
					if (!v == '') {
						return true;
					} else {
						res.json({
							message: 'Username should not be empty'
						});
						return false;
					}
				},
				message: 'Username should not be.....'
			},

		},
		pass: {
			type: String,
			required: true,
		},
		addresses: [{
			type: mongoose.Schema.Types.ObjectId,
			ref: 'address_data'
		}]
	}, {

		strict: false,
		collection: 'users'
	});

	users_schema.plugin(uniqueValidator, {
		type: 'mongoose-unique-validator'
	});

	var users = module.exports = conn.model("users", users_schema)

	module.exports.getUserByUsername = function (username, callback) {
		users.findOne({
			username: username,
		}, function (err, login_res) {
			if (err) throw err;
			callback(null, login_res)
		})
	}
	module.exports.getUserById = function (id, callback) {
		users.findById(id, callback);
	}
	module.exports.comparePassword = function (inPass, storePass, callback) {
		var new_pass = md5(inPass);
		if (new_pass == storePass) {
			callback(null, 1);
		} else {
			// console.log("pass he galat h...", storePass)
			callback(null, 0)
		}
	}