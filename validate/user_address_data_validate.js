var express = require('express');
var router = express.Router();

var validate = (function (body) {
    console.log(body)
    var address = body.address;
    var city = body.city;
    var state = body.state;
    var pincode = body.pincode;
    var phone = body.phone;
    console.log(address)
    if (address == '' || address == null) {
        return new Error("Plz, enter the value of address")
    } else if (city == "" || city == null) {
        return new Error("Plz, enter the value of city")
    } else if (state == "" || state == null) {
        return new Error("Plz, enter the value of state")
    } else if (pincode == "" || pincode == null) {
        return new Error("Plz, enter the value of pincode")
    } else if (phone == "" || phone == null) {
        return new Error("Plz, enter the value of phone")
    } else {
        return body;
    }
});
module.exports = validate;